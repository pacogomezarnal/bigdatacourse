source activate_env
./install_hdfs.sh 
./start_hadoop.sh
hdfs dfs -ls /

/*APACHE PIG*/
pig -x local
/*CARGA DE DATOS*/
Students = LOAD 'datasets/students.tsv' USING PigStorage('\t','-noschema') AS (student_id:Long,name:Chararray,surname:Chararray,gender:Chararray,age:Int);
Grades = LOAD 'datasets/grades.tsv' USING PigStorage('\t','-noschema') AS (student_id:Long,course:Chararray,grade:Double);

/*EJERCICIO <25*/
Students25 = FOREACH Students GENERATE student_id, name, surname, gender, age, ( age<25 ? true : false ) as Edad25;

/*EJERCICIO SPLIT*/
SPLIT Students INTO StudentsUnder25 IF age<25, StudentsUnder30 IF age<30, OtherStudents OTHERWISE;

/*EJERCICIO AGRUPACION*/
StudentGr= GROUP Students BY gender;

/*EJERCICIO NEST*/
StudentNested= FOREACH StudentGr{
Information = FOREACH Students GENERATE name, surname;
GENERATE group AS gender, Information AS student_info;}

/*EJERCICIO NEST 2*/
StudentsByAgeV2= FOREACH Students GENERATE student_id, name, surname, gender, age, ( age<25 ? 'menor25' : ( age>30 ? 'mayor30' : 'entre25-30' ) ) as catEdad;
StudentsByAgeG= GROUP StudentsByAgeV2 BY catEdad;
StudentsByAgeNested= FOREACH StudentsByAgeG{
Information = FOREACH StudentsByAgeV2 GENERATE name, surname;
GENERATE group AS catEdad, Information AS student_info;}

/*INNER JOIN*/
StundentsGRades = JOIN Students BY student_id, Grades BY student_id;
StundentsGradesALL = JOIN Students BY student_id LEFT, Grades BY student_id;

/*CROSS*/
StundentCross = CROSS Students,Grades;
StudentCrossV2 = JOIN Students BY 1, Grades BY 1;

/*EJERCICIO FINAL*/
StudentsWithGrades= JOIN Students BY student_id LEFT, Grades BY student_id;
StudentsWithGradesG= GROUP StudentsWithGrades BY Students::student_id;
StudentsWithGradesNested= FOREACH StudentsWithGradesG{
InformationV1 = FOREACH StudentsWithGrades GENERATE Grades::course AS course, Grades::grade AS grade;
InformationV2 = ORDER InformationV1 BY grade DESC;
InformationV3 = LIMIT InformationV2 3;
GENERATE group AS student_id, InformationV3 AS student_info;}


