from pyspark import SparkContext
from pyspark import SparkConf

import sys
import math
import random

def fitness(point):
	(x,y) = point
	return pow( 1-x, 2) + 100 * pow(y-x**2,2)

def generate_random_solution():
	return (random.uniform(-4,4),random.uniform(-4,4))

def get_neighbour(point,alpha):
	(x,y)=point
	derivx = -2*(1-x)-400*x*(y-x**2)
	derivy = 200*x*(y-x**2)
	return (x-alpha*derivx,y-alpha*derivy)

def annealing_step(point,alpha,T):
	point2 = get_neighbour(point,alpha)
	f1 = fitness(point)
	f2 = fitness(point)
	delta = f2-f1
	if delta < 0 :
		return point2
	elif random.random() < math.exp(-delta/T) :
		return point2
	return point

T0 = 5000
T = T0
epsilon = 0.1
alphaT = 0.9
alpha = 0.00001
conf = SparkConf().setAppName("Multipoint Annealing").setMaster("local")
sc = SparkContext(conf=conf)

points = sc.parallelize( range(1000) )
points = points.map( lambda x : generate_random_solution() )

k = 0
while T > epsilon :
	points = points.map( lambda x: annealing_step(x,alpha,T) )
	T = T*alphaT
	k=k+1	

min_point = points.map( lambda x: (fitness(x),x) ).min()
print("Minimum point is ", min_point, " found after ", k ,"iterations")
