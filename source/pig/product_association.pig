
/* Reading input. I use 2 parameters to provide dynamic files */
Products = LOAD '$input_products' USING PigStorage( '\t', '-noschema' ) AS ( product_id: Long, product_status: Chararray );

Purchases = LOAD '$input_purchases' USING PigStorage( '\t', '-noschema' ) AS ( user_id: Long, product_id: Long, price: Double, date: Chararray );

-- Keep products that are still valid
Products = FILTER Products BY product_status == 'ok';

-- We do not need all the information from the purchases, project data
Purchases = FOREACH Purchases GENERATE user_id, product_id;
-- Keep unique purchases by the user
Purchases = DISTINCT Purchases;

-- Purchases from products that are still valid
ValidPurchases = JOIN Purchases BY product_id, Products BY product_id;
ValidPurchases= FOREACH ValidPurchases GENERATE Purchases::product_id AS product_id, user_id AS user_id;

-- Group purchases from the same product
ValidPurchasesGr = GROUP ValidPurchases BY product_id;
ValidPurchasesGr = FOREACH ValidPurchasesGr {
	ListUsers = FOREACH ValidPurchases GENERATE user_id;
	ListUsers = ORDER ListUsers BY user_id;
	GENERATE group AS product_id, ListUsers AS list_users;
};

-- Another copy of the relationship
ValidPurchasesGr2 = FOREACH ValidPurchasesGr GENERATE product_id, list_users;

-- Cartesian product
ProductAssociation = JOIN ValidPurchasesGr BY 1, ValidPurchasesGr2 BY 1;
-- Remove cartesian product of same elements
ProductAssociation = FILTER ProductAssociation BY ValidPurchasesGr::product_id != ValidPurchasesGr2::product_id;

-- Calculate most of the product association terms
ProductAssociation = FOREACH ProductAssociation GENERATE ValidPurchasesGr::product_id AS first_product_id, ValidPurchasesGr2::product_id AS second_product_id, (Double) COUNT(ValidPurchasesGr::list_users) - COUNT( SUBTRACT( ValidPurchasesGr::list_users, ValidPurchasesGr2::list_users ) ) AS xandy, COUNT(ValidPurchasesGr::list_users) AS x, (Double) COUNT( SUBTRACT( ValidPurchasesGr2::list_users, ValidPurchasesGr::list_users ) ) AS notxandy;

-- Calculate all the users that have purchased
UniqueUsers = FOREACH ValidPurchases GENERATE user_id;
UniqueUsers = DISTINCT UniqueUsers;
UniqueUsersGr = GROUP UniqueUsers BY 1;
UniqueUsersGr = FOREACH UniqueUsersGr GENERATE COUNT(UniqueUsers) AS all_users;

-- Join with Product Association
ProductAssociation = JOIN UniqueUsersGr BY 1, ProductAssociation BY 1 USING 'skewed';
ProductAssociation = FOREACH ProductAssociation GENERATE first_product_id, second_product_id, xandy, x, notxandy, all_users - x AS notx;
ProductAssociation = FILTER ProductAssociation BY x != 0 AND notxandy != 0;
ProductAssociation = FOREACH ProductAssociation GENERATE first_product_id, second_product_id, (xandy * notx)/(x*notxandy) AS association;
ProductAssociation = FILTER ProductAssociation BY association > 0; 

STORE ProductAssociation INTO '$output' USING PigStorage( '\t', '-schema' );
